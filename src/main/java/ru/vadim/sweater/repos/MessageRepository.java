package ru.vadim.sweater.repos;

import org.springframework.data.repository.CrudRepository;
import ru.vadim.sweater.domain.Message;

import java.util.List;

public interface MessageRepository extends CrudRepository<Message, Integer> {

    List<Message> findByTag(String tag);
}
